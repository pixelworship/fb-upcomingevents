//
//  Upcoming_EventsTests.swift
//  Upcoming EventsTests
//
//  Created by Matthew Mourlam on 11/6/18.
//  Copyright © 2018 Pixel Worship, LLC. All rights reserved.
//

import XCTest
@testable import Upcoming_Events

class Upcoming_EventsTests: XCTestCase {
    var eventsViewModel: EventsViewModel?
    
    override func setUp() {
        eventsViewModel = EventsViewModel()
    }
    
    func testDateExtension() {
        let date = Date(eventDateString: "November 1, 2018 6:00 AM")
        
        XCTAssertNotNil(date)
        XCTAssert(date?.headerViewDateString() == "Thursday, November 1")
        XCTAssert(date?.yearMonthDayString() == "2018-11-01")
        XCTAssert(date?.timeString() == "6:00 AM")
    }

    func testProcessingAlgorithm() {
        let groupedEvents = eventsViewModel?.groupedEvents
        
        XCTAssert(groupedEvents?.count == 7)
        
        XCTAssert(groupedEvents?.first?.count == 3)
        XCTAssert(groupedEvents?.first?[0].title == "Bicycling with Friends")
        XCTAssert(groupedEvents?.first?[1].title == "Yoga")
        XCTAssert(groupedEvents?.first?[2].title == "Local Pub with Friends")
        
        XCTAssert(groupedEvents?.first?[0].hasConflict == false)
        XCTAssert(groupedEvents?.first?[1].hasConflict == false)
        XCTAssert(groupedEvents?.first?[2].hasConflict == false)
        
        XCTAssert(groupedEvents?[1][0].hasConflict == true)
        XCTAssert(groupedEvents?[1][1].hasConflict == true)
        
        XCTAssert(groupedEvents?.last?.count == 3)
        XCTAssert(groupedEvents?.last?[0].title == "Birthday Party")
        XCTAssert(groupedEvents?.last?[1].title == "Dentist Appointment")
        XCTAssert(groupedEvents?.last?[2].title == "Evening Picnic")
        XCTAssert(groupedEvents?.last?[0].hasConflict == true)
        XCTAssert(groupedEvents?.last?[1].hasConflict == true)
        XCTAssert(groupedEvents?.last?[2].hasConflict == true)
    }
}
