//
//  EventsViewModel.swift
//  Upcoming Events
//
//  Created by Matthew Mourlam on 11/6/18.
//  Copyright © 2018 Pixel Worship, LLC. All rights reserved.
//

import UIKit

class EventsViewModel {
    private(set) var groupedEvents = [[Event]]()
    
    var numberOfSections: Int {
        return groupedEvents.count
    }
    
    init() {
        guard let events = MockEventDataStore().loadEvents() else {
            fatalError("Failed to load events from mock data store")
        }
        
        self.groupedEvents = processedEvents(from: events)
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return groupedEvents[section].count
    }
    
    func cell(atIndexPath indexPath: IndexPath, tableView: UITableView) -> EventCell {
        let section = groupedEvents[indexPath.section]
        let event = section[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: EventCell.self), for: indexPath) as? EventCell else {
            fatalError("Failed to dequeue \(EventCell.self) cell")
        }
        
        cell.titleLabel.text = event.title
        cell.startDateLabel.text = event.startDate.timeString()
        cell.endDateLabel.text = event.endDate.timeString()
        cell.hasEventConflict = event.hasConflict
        
        return cell
    }
    
    func headerView(forSection section: Int) -> UIView {
        guard let firstItem = groupedEvents[section].first else { return UIView() }
        
        let headerView = EventHeaderView(frame: .zero)
        headerView.dateString = firstItem.startDate.headerViewDateString()
        
        return headerView
    }
}

// Algorithm for processing our raw events into sorted, grouped and processed view models

// Basic algorithm steps
// 1) sort our list of raw events by start timestamp - O(n * log n)
// 2) group sorted events by date - O(n)
// 3) iterate over each event in our group and check if this event conflicts with next upcoming event by comparing it's end timestamp to next event's start timestamp
// if there is a conflict, we'll mark current event as having a conflict, plus setting a flag letting our next iteration know that next event also has a conflict (since we
// We keep a reference to the largest end timestamp we've seen so far so we can also compare events that overlap multiple events

// Tradeoffs & assumptions:
// We assume all events are contained in a single day - meaning event for Thursday won't overlap into Friday, the next day or any other day
//      (we could simply not reset currentMaxEndTime for each group to allow multi-day spanning
// This particular algorithm only works based on the requirement "There is *some* indication that an event has a conflict"
// If we needed to know *which* events conflicts with *which* events rather than just whether or not this even *has* a conflict, I would
// have to take a different approach - I believe we could still do it in n * log n but would need to decouple start and end times

// Space complexity: O(n)
// Runtime complexity: O(n * log n)
// O(n * log n) comes from our initial sort - swift's built-in introsort. For processing, we iterate over entire list of events 2x's =  2n + n * log (n)   =   n * log (n)
// I could have optimized algorithm even further and only iterating over entire list 1x instead of 2x by grouping inside same loop as processing but I thought the code was much easier to read
// and understand by separating grouping of events with processing overlaps. This is a personal preference but again, could be slightly optimized.

extension EventsViewModel {
    
    // Note: requires input of events to be sorted first!
    
    func groupEventsByDate(from events: [Event]) -> [[Event]] {
        var groups = [[Event]]()
        
        // Contains string of date we are currently processing
        // if there is a mismatch with this and current event, we need to create a new group
        var currentDate: String?
        
        // Index of group we're currently working with
        var currentGroupIndex = -1
        
        events.forEach { event in
            // Make sure this event date matches the group event date we're currently working with
            // If not, let's create a new group
            if event.startDate.yearMonthDayString() != currentDate {
                groups.append([Event]())
                currentGroupIndex += 1
            }
            
            currentDate = event.startDate.yearMonthDayString()
            groups[currentGroupIndex].append(event)
        }
        
        return groups
    }
    
    func processedEvents(from events: [Event]) -> [[Event]] {
        // Sort events by start date
        let sortedEvents = events.sorted { $0.startTimestamp < $1.startTimestamp }
        
        // Container for all of our processed groups of event view models
        var processedGroupedEvents = [[Event]]()
        
        // Iterate through every group
        for group in groupEventsByDate(from: sortedEvents) {
            // This should never happen but just in case this particular group doesn't contain any events,
            // just move onto the next without adding it to our processed groups
            guard group.count > 0  else { continue }
            
            var processedGroup = [Event]()
            
            // Hold our maximum end date timestamp for events overlapping multiple other events
            var currentMaxEndTime: TimeInterval = 0
            
            // Iterate through every event in group
            for index in 0 ..< group.count {
                // Reference to current event we want to process
                var currentEvent = group[index]
                
                // Compare this events end time to our next events start time
                // If there is overlap, we have a conflict
                if currentEvent != group.last {
                    var nextEvent = group[index + 1]
                
                    if currentEvent.endTimestamp > nextEvent.startTimestamp {
                        currentEvent.markWithConflict()
                        nextEvent.markWithConflict()
                    }
                }
                
                // Does this events start time fall into our current max end time (mutli-event spanning event)?
                if currentEvent.startTimestamp < currentMaxEndTime {
                    currentEvent.markWithConflict()
                }
                
                // Set new maximum end timestamp
                currentMaxEndTime = max(currentMaxEndTime, currentEvent.endTimestamp)
                
                processedGroup.append(currentEvent)
            }
            
            processedGroupedEvents.append(processedGroup)
        }
        
        return processedGroupedEvents
    }
}
