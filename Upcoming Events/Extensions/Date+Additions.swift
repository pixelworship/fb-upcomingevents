//
//  Date+Additions.swift
//  Upcoming Events
//
//  Created by Matthew Mourlam on 11/6/18.
//  Copyright © 2018 Pixel Worship, LLC. All rights reserved.
//

import Foundation

extension Date {
    // Initializer with date format that conforms to mock.json model dates
    init?(eventDateString: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "MMMM d, yyyy h:mm a"

        guard let date = dateFormatter.date(from: eventDateString) else { return nil }
        self = date
    }
    
    // Friday, November 9 format used for header view
    func headerViewDateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM d"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter.string(from: self)
    }
    
    // YYYY-MM-DD format used for grouping dates
    func yearMonthDayString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter.string(from: self)
    }
    
    // Used to display time in "12:58 PM" format
    func timeString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter.string(from: self)
    }
}
