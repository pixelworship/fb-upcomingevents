//
//  Event.swift
//  Upcoming Events
//
//  Created by Matthew Mourlam on 11/6/18.
//  Copyright © 2018 Pixel Worship, LLC. All rights reserved.
//

import Foundation

struct Event {
    private(set) var title: String
    private(set) var startDate: Date
    private(set) var endDate: Date
    private(set) var hasConflict = false
    
    var startTimestamp: TimeInterval {
        return startDate.timeIntervalSince1970
    }
    
    var endTimestamp: TimeInterval {
        return endDate.timeIntervalSince1970
    }
    
    mutating func markWithConflict() {
        hasConflict = true
    }
}

extension Event: Decodable {
    enum EventDecoderError: Error {
        case unknown
        case badDateString
    }
    
    enum EventKeys: String, CodingKey {
        case title = "title"
        case start = "start"
        case end = "end"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: EventKeys.self)
        let title = try container.decode(String.self, forKey: .title)
        let startDateString = try container.decode(String.self, forKey: .start)
        let endDateString = try container.decode(String.self, forKey: .end)
   
        guard let startDate = Date(eventDateString: startDateString),
            let endDate = Date(eventDateString: endDateString) else {
                throw EventDecoderError.badDateString
        }
        
        self.init(title: title, startDate: startDate, endDate: endDate, hasConflict: false)
    }
}

extension Event: CustomStringConvertible {
    var description: String {
        return "\(title): \(startDate) - \(endDate)\n"
    }
}

extension Event: Equatable {
    static func == (l: Event, r: Event) -> Bool {
        return l.title == r.title && l.startDate == r.startDate && l.endDate == r.endDate
    }
}
