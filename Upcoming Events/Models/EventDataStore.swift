//
//  EventDataStore.swift
//  Upcoming Events
//
//  Created by Matthew Mourlam on 11/7/18.
//  Copyright © 2018 Pixel Worship, LLC. All rights reserved.
//

import Foundation

protocol EventDataStore {
    func loadEvents() -> [Event]?
}

class MockEventDataStore: EventDataStore {
    func loadEvents() -> [Event]? {
        guard let jsonPath = Bundle.main.path(forResource: "mock", ofType: "json"),
            let jsonData = FileManager.default.contents(atPath: jsonPath) else { return nil }
        return try? JSONDecoder().decode([Event].self, from: jsonData)
    }
}
