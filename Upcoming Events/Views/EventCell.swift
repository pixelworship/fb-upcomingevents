//
//  EventCell.swift
//  Upcoming Events
//
//  Created by Matthew Mourlam on 11/6/18.
//  Copyright © 2018 Pixel Worship, LLC. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var horizontalDividerView: UIView!
    @IBOutlet weak var conflictPatternView: UIView!
    
    @IBOutlet weak var leftMarginWidthConstraint: NSLayoutConstraint!
    
    // Sets the look of the cell based on whether or not event has a conflict
    // Red and indented if conflict exists
    
    var hasEventConflict: Bool = false {
        didSet {
            if hasEventConflict {
                leftMarginWidthConstraint.constant =  originalMarginWidthConstant + 20
                horizontalDividerView.backgroundColor = Theme.conflictRed
            } else {
                leftMarginWidthConstraint.constant = originalMarginWidthConstant
                horizontalDividerView.backgroundColor = Theme.facebookBlue
            }
            
            conflictPatternView.isHidden = !hasEventConflict
            
            contentView.layoutIfNeeded()
        }
    }
    
    private var originalMarginWidthConstant: CGFloat = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Hold our original constraint constant from storyboard
        // this way we can modify padding inside storyboard without making any changes to our code
        originalMarginWidthConstant = leftMarginWidthConstraint.constant
        
        if let backgroundPatternImage = UIImage(named: "Conflict Background Pattern") {
            conflictPatternView.backgroundColor = UIColor(patternImage: backgroundPatternImage)
        } else {
            fatalError("Missing asset: \"Conflict-BackgroundPattern\"")
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        // Reset views back to default state
        hasEventConflict = false
    }
}
