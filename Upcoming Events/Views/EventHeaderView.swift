//
//  EventHeaderView.swift
//  Upcoming Events
//
//  Created by Matthew Mourlam on 11/7/18.
//  Copyright © 2018 Pixel Worship, LLC. All rights reserved.
//

import UIKit

class EventHeaderView: UIView {
    let dateLabel = UILabel()
    var dateString: String? {
        didSet {
            dateLabel.text = dateString
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Setup event header view with simple label and basic constraints
        
        backgroundColor = Theme.lightGrey
        
        addSubview(dateLabel)
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        dateLabel.textColor = Theme.fontGrey
        
        dateLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 14).isActive = true
        dateLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 14).isActive = true
        dateLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        dateLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("\(#function) not supported")
    }
}
