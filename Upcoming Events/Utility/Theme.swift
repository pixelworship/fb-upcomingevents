//
//  Theme.swift
//  Upcoming Events
//
//  Created by Matthew Mourlam on 11/7/18.
//  Copyright © 2018 Pixel Worship, LLC. All rights reserved.
//

import UIKit

// Theme contains colors reused throughout the app

class Theme {
    static let lightGrey = UIColor(red: 230 / 255, green: 230 / 255, blue: 230 / 255, alpha: 1)
    static let fontGrey = UIColor(red: 107 / 255, green: 107 / 255, blue: 107 / 255, alpha: 1)
    static let facebookBlue = UIColor(red: 60 / 255, green: 91 / 255, blue: 152 / 255, alpha: 1)
    static let conflictRed = UIColor(red: 206 / 255, green: 4 / 255, blue: 4 / 255, alpha: 1)
}
